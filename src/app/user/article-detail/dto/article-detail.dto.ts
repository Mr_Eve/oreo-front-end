export class ArticleDetailDTO {
  id: number;
  name: string;
  createTime: Date;
  updateTime: Date;
  content: string;
  likeAmount: number;
  commentAmount: number;
  collectNumber: number;
}
