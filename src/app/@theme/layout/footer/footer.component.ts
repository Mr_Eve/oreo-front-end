import { Component } from '@angular/core';

@Component({
  selector: 'app-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
    Powered by <b><a href="javascript:;">Eve</a></b> ©2019 | 如需转载请标明出处 | 前端交流群:925528845
  `,
})
export class AppFooterComponent {
}
